import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state() {
    return {
      orders: [],
    };
  },
  getters: {
    orders(state) {
      return state.orders;
    },
  },
  mutations: {
    setOrders(state, payload) {
      state.orders = payload;
    },
    removeOrder(state, id) {
      state.orders = state.orders.filter((order) => order.id !== id);
    },
  },
  actions: {
    getOrders({ commit }) {
      fetch('cards.json')
        .then((data) => data.json())
        .then((res) => {
          const orders = res.map((order) => {
            let type;
            switch (order.type) {
              case 'Delivery':
                type = 'Курьерская доставка';
                break;
              case 'Pick-point':
                type = 'Доставка в пункт выдачи';
                break;
              case 'Pickup':
              default:
                type = 'Самовывоз';
                break;
            }

            const parse = new Date(Date.parse(order.creationDate));
            const creationDate = `${parse.toLocaleDateString('ru-RU')} ${parse.toTimeString().split(' ')[0]}`;
            return {
              ...order,
              type,
              creationDate,
            };
          });

          commit('setOrders', orders);
        });
    },
  },
});

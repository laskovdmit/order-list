import Vue from 'vue';
import VueRouter from 'vue-router';
import TableList from '@/components/TableList.vue';
import WidgetList from '@/components/WidgetList.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/table',
  },
  {
    path: '/table',
    name: 'TableList',
    component: TableList,
  },
  {
    path: '/widget',
    name: 'WidgetList',
    component: WidgetList,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
